# app/controllers/payments_controller.rb

class PaymentsController < ApplicationController
  require 'active_merchant'

  ActiveMerchant::Billing::Base.mode = :test

  def new
  end

  def create

    if params[:payment_method] == 'paypal' || params[:payment_method].blank?

      # paypal
      gateway = ActiveMerchant::Billing::PaypalGateway.new(
          :login => "sb-ea78l27865931_api1.business.example.com",
          :password => "5KYJN6CBBFK2PNWH",
          :signature => "AIrE4g5nHmtxLLEDSOgcwHiEj9WnAjuAQ5ptzd3cNUPlePrhG4cx97jJ"
        
      )
      # credit_card = ActiveMerchant::Billing::CreditCard.new(
      #   number: '4111111111111111',
      #   month: '8',
      #   year: '2024',
      #   verification_value: '123',
      #   first_name: 'Bob',
      #   last_name: 'Bobsen'
      # )

      credit_card = ActiveMerchant::Billing::CreditCard.new(
        number: '4032030457600576',
        month: '9',
        year: '2027',
        verification_value: '123',
        first_name: 'John',
        last_name: 'Doe'
      )


      # PaypalExpressGateway: thanh toán qua tài khoản paypal còn cái trên là thẻ

      # paypal_exporess = ActiveMerchant::Billing::PaypalExpressGateway.new(
      #   :login => "sb-ea78l27865931_api1.business.example.com",
      #   :password => "5KYJN6CBBFK2PNWH",
      #   :signature => "AIrE4g5nHmtxLLEDSOgcwHiEj9WnAjuAQ5ptzd3cNUPlePrhG4cx97jJ"
      # )

      # response = paypal_exporess.setup_purchase(1000,
      #   ip: request.remote_ip,
      #   return_url: root_url,
      #   cancel_return_url: root_url
      # )
      # return redirect_to paypal_exporess.redirect_url_for(response.token)


    elsif params[:payment_method] == 'stripe'

      gateway = ActiveMerchant::Billing::StripeGateway.new(login: 'sk_test_51O2zTzGO1vIq7wDuYK6xMUo8AOxjnLGHsOB0JbVMldyUD5JZ20zwKo9ciAEZpxorxmtOhBrdlEo0LS1nSMtKG39i00j1Rix28y')
      credit_card = 'tok_visa' # Thay thế bằng mã thông báo thử nghiệm thích hợp

    end


    purchase_options =  {
        :ip => '127.0.0.1',
        :currency => 'USD',
        :billing_address => {
          :name     => "Sun Staff",
          :address1 => "13F KNE Pham Hung street",
          :city     => "HaNoi",
          :state    => "HN",
          :country  => "VI",
          :zip      => "100000"
        }
      }

    # lỗi  10414

    response = gateway.purchase(1000, credit_card, purchase_options)

    if response.success?
      # flash[:success] = 'Thanh toán thành công!'
      render json: {
        msg: 'Thanh toán thành công!',
        response: response
      }
    else
      render json: {
        msg: 'Thanh toán thất bại: ' + response.message,
        response: response
      }
      # flash[:error] = 'Thanh toán thất bại: ' + response.message
    end
    # redirect_to root_path
  end

  def refund
    # Chọn chế độ test
    ActiveMerchant::Billing::Base.mode = :test

    # Thay thế các thông tin của cổng thanh toán thực tế
    gateway = ActiveMerchant::Billing::PaypalGateway.new(
      :login => "sb-ea78l27865931_api1.business.example.com",
      :password => "5KYJN6CBBFK2PNWH",
      :signature => "AIrE4g5nHmtxLLEDSOgcwHiEj9WnAjuAQ5ptzd3cNUPlePrhG4cx97jJ"
    )

    # Thực hiện hoàn tiền
    response = gateway.refund(
      1000,
      params[:transaction_id]
    )

    if response.success?
      # puts 'Hoàn tiền thành công!'
      return render json: {
        msg: 'Hoàn tiền thành công!',
        response: response
      }
    else
      return render json: {
        msg: 'Hoàn tiền thất bại: ' + response.message,
        response: response
      }
      # puts 'Hoàn tiền không thành công. Vui lòng thử lại sau.'
    end
  end
end
