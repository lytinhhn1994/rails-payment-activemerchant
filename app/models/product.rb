class Product < ApplicationRecord
  monetize :price_cents, :allow_nil => true
end
