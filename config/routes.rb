Rails.application.routes.draw do
  resources :payments, only: [:new, :create]
  post '/refund', to: 'payments#refund', as: 'refund_payments'
  root to: 'payments#new' # Thay 'home#index' bằng controller và action chính xác của trang chủ của bạn
end
