## Yêu cầu
bạn phải cài ruby, rails, postgres

## Chuẩn bị
**1. Tạo tài khoản paypal**
Bạn đăng ký 1 account tại https://www.paypal.com/, 
sau đó có thể dùng để đăng nhập vào trang dành cho developer https://developer.paypal.com

**2. Tạo account**
Ở đây bạn có sẵn 2 account paypal sandbox để test:
- account personal dùng để đăng nhập khi thanh toán
- account business được liên kết với app vừa tạo dùng để nhận tiền
> Note: 2 account sandbox testing bạn có thể đăng nhập vào trang https://www.sandbox.paypal.com/,
đây là trang dùng để test cho trang chủ https://www.paypal.com/

**3. Ví dụ tài khoản test**

web đăng nhập tài khoản sanbox
https://www.sandbox.paypal.com

Tài khoản chủ shop
email: sb-ea78l27865931@business.example.com
pass: L>it?4*z

Tài khoản người mua
email: sb-o2vrg20207022@personal.example.com
pass: ].In%08S


Thẻ demo
https://developer.paypal.com/tools/sandbox/card-testing


## Cài đặt
Chạy dự án
```sh
cd project
bundle
rails db:create
rails db:migrate
rails server
```

# Active Merchant
## Installation
## Usage
This simple example demonstrates how a purchase can be made using a person's
credit card details.

```ruby
require 'active_merchant'

# Use the TrustCommerce test servers
ActiveMerchant::Billing::Base.mode = :test

gateway = ActiveMerchant::Billing::TrustCommerceGateway.new(
            :login => 'TestMerchant',
            :password => 'password')

# ActiveMerchant accepts all amounts as Integer values in cents
amount = 1000  # $10.00

# The card verification value is also known as CVV2, CVC2, or CID
credit_card = ActiveMerchant::Billing::CreditCard.new(
                :first_name         => 'Bob',
                :last_name          => 'Bobsen',
                :number             => '4242424242424242',
                :month              => '8',
                :year               => Time.now.year+1,
                :verification_value => '000')

# Validating the card automatically detects the card type
if credit_card.validate.empty?
  # Capture $10 from the credit card
  response = gateway.purchase(amount, credit_card)

  if response.success?
    puts "Successfully charged $#{sprintf("%.2f", amount / 100)} to the credit card #{credit_card.display_number}"
  else
    raise StandardError, response.message
  end
end
```


# money-rails
https://github.com/RubyMoney/money-rails 

## config
```ruby
gem "money-rails"
rails g money_rails:initializer

# sẽ sinh ra file config/initializers/money.rb
MoneyRails.configure do |config|
  config.default_currency = :usd
  config.add_rate "USD", "CAD", 1.24515
  config.add_rate "USD", "VND", 24000
end
```

## Chuyển đổi tiền tệ
```ruby
r# Phép toán
irb(main):011:0> Money.new(800, "USD") + Money.new(200, "USD")
=> #<Money fractional:1000 currency:USD>

irb(main):012:0> Money.new(800, "USD") - Money.new(200, "USD")
=> #<Money fractional:600 currency:USD>

irb(main):013:0> Money.new(800, "USD") / 2
=> #<Money fractional:400 currency:USD>

irb(main):014:0> Money.new(800, "USD") * 2
=> #<Money fractional:1600 currency:USD>

# Chuyển đổi tiền tệ

irb(main):001:0> Money.new(1000, "USD").exchange_to("CAD")            # config.add_rate "USD", "CAD", 1.24515
=> #<Money fractional:1245 currency:CAD

irb(main):002:0> Money.new(9800, "USD").exchange_to("VND")       #config.add_rate "USD", "VND", 22727
=> #<Money fractional:2227246 currency:VND>

# Chuyển đổi định dạng format
irb(main):003:0> Money.new(2300, "USD").format
=> "$23.00"

irb(main):005:0> Money.new(200, "EUR").format
=> "€2.00"
```
